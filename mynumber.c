#include <stdio.h>

int IsPrime(int n)
{
	int end = n/2;
	int i;

	if ( n <=1 )
		return 0;
	for ( i = 2 ; i <= end ; i++){
		if ( (n % i) == 0 )
			return 0;
	}
	return 1;
}

int main()
{
	int i;
	for (i = 1 ; i <= 100; i++){
		if(i % 2 == 1)
			printf("%d  ",i);
	}
	printf("\n\n");
	for (i = 1 ; i <= 100; i++){
		if(i % 2 == 0)
			printf("%d  ",i);
	}
	printf("\n\n");
	for (i = 1 ; i <= 100 ; i++){
		if(IsPrime(i))
			printf("%d  ",i);
	}
	printf("\n");	
	return 0;
}
// last version
